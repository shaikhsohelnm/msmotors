import React, { useState, useEffect, useRef } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import styled from "@emotion/styled";
import fitneddimg from "../carimage/car16.png";
import fitneddimgSecond from "../carimage/car10.png";
import pizza1 from "../carimage/car13.png";
import pizza2 from "../carimage/car1.png";
import pizza3 from "../carimage/car17.png";
import pizza4 from "../carimage/car6.png";
import pizza5 from "../carimage/car5.png";
import pizza6 from "../carimage/car10.png";
import pizza7 from "../carimage/car12.png";
import thirdcarouseone from "../carimage/car24.png";
import thirdcarousetwo from "../carimage/car20.png";
import thirdcarousethree from "../carimage/car21.png";
import thirdcarousefour from "../carimage/car23.png";
import thirdcarousefive from "../carimage/car24.png";
import fourthcarouseone from "../carimage/car5.png";
import fifthcarouseone from "../carimage/car23.png";
import fifthcarousetwo from "../carimage/car22.png";
import fifthcarousethree from "../carimage/car21.png";
import sixthcarouseone from "../carimage/car23.png";
import sixthcarousetwo from "../carimage/car20.png";
import seventhcarouseone from "../carimage/car24.png";
import seventhcarousetwo from "../carimage/car21.png";
import seventhcarousethree from "../carimage/car20.png";

import { gsap, Power3 } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import LogoMS from "../svg/LogoMS";
gsap.registerPlugin(ScrollTrigger);

const WrapperStyled = styled.div`
  position: relative;
  a {
    text-decoration: none;
    color: #fff;
  }
  margin: 0;
  padding: 0;

  .carouselwrapper {
    padding: 0;
    margin: 0;
    border: 0;
    outline: 0;
    width: 100dvw;
    height: 100dvh;
    display: grid;
    grid-template-columns: 1fr;
    grid-template-rows: 1fr 1fr;
    .image {
      background: #141347;
      overflow: hidden;
      height: 55dvh;
      .common-img {
        width: 50%;
      }
      .fitnessone {
        position: absolute;
        top: 95px;
        left: 0;
        opacity: 1;
      }
      .fitnesssecond {
        position: absolute;
        top: 0%;
        right: 0;
        width: 40%;
      }
    }
    .content {
      background: #0e103a;
      height: 45dvh;
      padding-left: 6%;
      overflow: hidden;

      h1,
      p,
      a {
        margin: 0;
        padding: 0;
        color: #fff;
      }
      h1 {
        margin-bottom: 6px;
        font-size: 16px;
        font-weight: 700;
      }
      p {
        margin-bottom: 30px;
        font-size: 14px;
      }
      button {
        border: 0;
        border-radius: 20px;
        padding: 6px 19px;
        font-size: 14px;
      }
    }
  }

  @media only screen and (min-width: 767px) {
    .carouselwrapper {
      display: grid !important;
      grid-template-columns: 1fr 1fr !important;
      grid-template-rows: 1fr !important;
      direction: rtl !important;
      .content,
      .image {
        height: 100dvh;
      }
    }
    .carouselwrapper .content {
      padding-top: 270px;
    }
    .carouselwrapper .image .fitnessone {
      top: 250px;
    }
  }

  .carouselsecond {
    .image {
      background: #0d121b;

      .pizza3 {
        left: 0;
        bottom: 0;
        width: 40%;
      }
      .pizza7 {
        top: 0;
        left: 0;
        width: 25%;
      }
      .pizza1 {
        left: 50%;
        top: 30%;
        width: 35%;
      }
      .pizza4 {
        bottom: 0;
        right: -70px;
        width: 40%;
      }
      .pizza5 {
        top: -223px;
        right: 50px;
        width: 40%;
      }
      .pizza6 {
        right: -139px;
        top: 30%;
        bottom: 0;
        width: 37%;
      }
      .pizza2 {
        bottom: 0;
        right: 82px;
        width: 32%;
      }
    }
    .content {
      background: #142136;
    }
  }

  .carouselfirst {
    .image {
      .fitneddimgSecond {
        left: 0;
        bottom: 0;
        width: 45%;
      }
    }
  }
  .carouselthird {
    .image {
      background: #53d0eb;

      .thirdcarouseone {
        top: -30px;
        left: 33%;
        width: 30%;
      }
      .thirdcarousetwo {
        left: 0;
        top: -159px;
        width: 30%;
      }
      .thirdcarousethree {
        bottom: -135px;
        left: 240px;
        width: 30%;
      }
      .thirdcarousefour {
        left: 0;
        bottom: -30px;
        width: 30%;
      }
      .thirdcarousefive {
        right: 0;
        top: 15%;
        width: 30%;
      }
    }
    .content {
      background: #135fda;
    }
  }

  .carouselfourth {
    .image {
      background: #05236c;
      .common-img {
        width: 100%;
      }
    }
    .content {
      background: #052889;
    }
  }
  .carouselfive {
    .image {
      background: #57b76a;
      .common-img {
        width: 50%;
      }
      .fifthcarouseone {
        bottom: -65px;
        right: 34px;
        width: 40%;
      }
      .fifthcarousetwo {
        left: 18%;
        bottom: -21px;
        width: 40%;
      }
      .fifthcarousethree {
      }
    }
    .content {
      background: #347344;
    }
  }

  .carouselsix {
    .image {
      background: #1f1b61;

      .sixthcarouseone {
        bottom: 0;
        left: 40px;
        width: 45%;
      }
      .sixthcarousetwo {
        right: 0;
        top: 0;
        width: 45%;
      }
    }
    .content {
      background: #9d43ba;
    }
  }
  .carouselseven {
    .image {
      background: #ceccd0;

      .seventhcarouseone {
        left: 0;
        top: 30%;
        width: 40%;
      }

      .seventhcarousetwo {
        right: 30px;
        top: 20%;
        width: 45%;
      }
      .seventhcarousethree {
        left: 0;
        top: 66%;
        width: 45%;
      }
    }
    .content {
      background: #6437a5;
    }
  }

  .slick-dots {
    display: block;
    position: absolute;
    bottom: 9px;
    li {
      margin: 0;
      padding: 0;
      width: 17px;
      height: 17px;
    }
  }
  .slick-vertical .slick-slide {
    border: 0;
  }

  svg {
    // background-color: transparent !important;
    width: 25%;
    position: absolute;
    top: 12%;
    left: 35%;
    border-radius: 50%;
    border: 0;
  }
  // position: relative;
  // a {
  //   text-decoration: none;
  //   color: #fff;
  // }
  // margin: 0;
  // padding: 0;

  // .carouselwrapper {
  //   padding: 0;
  //   margin: 0;
  //   border: 0;
  //   outline: 0;
  //   width: 100dvw;
  //   height: 100dvh;
  //   display: grid;
  //   grid-template-columns: 1fr;
  //   grid-template-rows: 1fr 1fr;
  //   .image {
  //     background: #336699;
  //     overflow: hidden;
  //     height: 55dvh;
  //     .common-img {
  //       width: 50%;
  //     }
  //     .fitnessone {
  //       position: absolute;
  //       top: 95px;
  //       left: 0;
  //       opacity: 1;
  //     }
  //     .fitnesssecond {
  //       position: absolute;
  //       top: 0%;
  //       right: 0;
  //       width: 70%;
  //     }
  //   }
  //   .content {
  //     background: #cccccc;
  //     height: 45dvh;
  //     padding-left: 6%;
  //     overflow: hidden;

  //     h1,
  //     p,
  //     a {
  //       margin: 0;
  //       padding: 0;
  //       color: #fff;
  //     }
  //     h1 {
  //       margin-bottom: 6px;
  //       font-size: 16px;
  //       font-weight: 700;
  //     }
  //     p {
  //       margin-bottom: 30px;
  //       font-size: 14px;
  //     }
  //     button {
  //       border: 0;
  //       border-radius: 20px;
  //       padding: 6px 19px;
  //       font-size: 14px;
  //     }
  //   }
  // }
  // @media only screen and (max-width: 767px) {
  //   h1 {
  //     color: #fff !important;
  //     font-size: 24px !important;
  //   }
  //   p {
  //     color: #333333 !important;
  //     font-size: 16px !important;
  //   }
  // }

  // @media only screen and (min-width: 767px) {
  //   .carouselwrapper {
  //     display: grid !important;
  //     grid-template-columns: 1fr 1fr !important;
  //     grid-template-rows: 1fr !important;
  //     direction: rtl !important;
  //     .content,
  //     .image {
  //       height: 100dvh;
  //     }
  //     h1 {
  //       color: #fff !important;
  //       font-size: 36px !important;
  //     }
  //     p {
  //       color: #333333 !important;
  //       font-size: 24px !important;
  //     }
  //   }
  //   .carouselwrapper .content {
  //     padding-top: 270px;
  //   }
  //   .carouselwrapper .image .fitnessone {
  //     top: 250px;
  //   }
  // }

  // .carouselsecond {
  //   .image {
  //     background: #cc9933;

  //     .pizza3 {
  //       left: 0;
  //       bottom: 0;
  //       width: 50%;
  //     }
  //     .pizza7 {
  //       top: -27px;
  //       left: 0;
  //       width: 50%;
  //     }
  //     .pizza1 {
  //       left: 33%;
  //       top: 22%;
  //       width: 35%;
  //     }
  //     .pizza4 {
  //       bottom: 0;
  //       right: -105px;
  //       width: 45%;
  //     }
  //     .pizza5 {
  //       top: -223px;
  //       right: 50px;
  //       width: 40%;
  //     }
  //     .pizza6 {
  //       right: -139px;
  //       top: 30%;
  //       bottom: 0;
  //       width: 37%;
  //     }
  //     .pizza2 {
  //       bottom: 0;
  //       right: 82px;
  //       width: 32%;
  //     }
  //   }
  //   .content {
  //     background: #6699cc;
  //   }
  // }

  // .carouselfirst {
  //   .image {
  //     .fitneddimgSecond {
  //       left: 0;
  //       bottom: 0;
  //       width: 70%;
  //     }
  //   }
  // }
  // .carouselthird {
  //   .image {
  //     background: #996633;

  //     .thirdcarouseone {
  //       top: -30px;
  //       left: 33%;
  //       width: 30%;
  //     }
  //     .thirdcarousetwo {
  //       left: 0;
  //       top: -159px;
  //       width: 30%;
  //     }
  //     .thirdcarousethree {
  //       bottom: -135px;
  //       left: 240px;
  //       width: 30%;
  //     }
  //     .thirdcarousefour {
  //       left: 0;
  //       bottom: -30px;
  //       width: 30%;
  //     }
  //     .thirdcarousefive {
  //       right: 0;
  //       top: 15%;
  //       width: 30%;
  //     }
  //   }
  //   .content {
  //     background: #99cc99;
  //   }
  // }

  // .carouselfourth {
  //   .image {
  //     background: #666666;
  //     .common-img {
  //       width: 100%;
  //     }
  //   }
  //   .content {
  //     background: #ffcccc;
  //   }
  // }
  // .carouselfive {
  //   .image {
  //     background: #cc6633;
  //     .common-img {
  //       width: 50%;
  //     }
  //     .fifthcarouseone {
  //       bottom: -65px;
  //       right: 34px;
  //       width: 40%;
  //     }
  //     .fifthcarousetwo {
  //       left: 18%;
  //       bottom: -21px;
  //       width: 40%;
  //     }
  //     .fifthcarousethree {
  //     }
  //   }
  //   .content {
  //     background: #9999cc;
  //   }
  // }

  // .carouselsix {
  //   .image {
  //     background: #336600;

  //     .sixthcarouseone {
  //       bottom: 0;
  //       left: 40px;
  //       width: 45%;
  //     }
  //     .sixthcarousetwo {
  //       right: 0;
  //       top: 0;
  //       width: 45%;
  //     }
  //   }
  //   .content {
  //     background: #ffcc99;
  //   }
  // }
  // .carouselseven {
  //   .image {
  //     background: #993366;

  //     .seventhcarouseone {
  //       left: 0;
  //       top: 30%;
  //       width: 40%;
  //     }

  //     .seventhcarousetwo {
  //       right: 30px;
  //       top: 20%;
  //       width: 45%;
  //     }
  //     .seventhcarousethree {
  //       left: 0;
  //       top: 20%;
  //       width: 45%;
  //     }
  //   }
  //   .content {
  //     background: #ccccff;
  //   }
  // }

  // .slick-dots {
  //   display: block;
  //   position: absolute;
  //   bottom: 9px;
  //   li {
  //     margin: 0;
  //     padding: 0;
  //     width: 17px;
  //     height: 17px;
  //   }
  // }
  // .slick-vertical .slick-slide {
  //   border: 0;
  // }

  // svg {
  //   // background-color: transparent !important;
  //   width: 25%;
  //   position: absolute;
  //   top: 12%;
  //   left: 37%;
  //   border-radius: 50%;
  //   border: 0;
  // }
`;

function Landing() {
  const [isVertical, setIsVertical] = useState(window.innerWidth > 767);
  const sliderRef = useRef(null);

  const [currentSlide, setCurrentSlide] = useState(0);

  useEffect(() => {
    const handleResize = () => {
      setIsVertical(window.innerWidth > 767);
    };

    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  useEffect(() => {
    const handleWheel = (e) => {
      if (sliderRef.current) {
        const delta = e.deltaY;

        if (delta > 0) {
          sliderRef.current.slickNext();
        } else {
          sliderRef.current.slickPrev();
        }
      }
    };

    window.addEventListener("wheel", handleWheel);

    return () => {
      window.removeEventListener("wheel", handleWheel);
    };
  }, [sliderRef]);

  const ref = useRef([]);

  const pararef = useRef([]);

  pararef.current = [];

  const anchorref = useRef([]);

  anchorref.current = [];

  ref.current = [];

  let carouseoneimgdown = useRef(null);
  let carouseoneimgup = useRef(null);

  let carousetwoimgone = useRef(null);
  let carousetwoimgtwo = useRef(null);
  let carousetwoimgthree = useRef(null);

  let carousetwoimgfour = useRef(null);
  let carousetwoimgfive = useRef(null);
  let carousetwoimgsix = useRef(null);
  let carousetwoimgseven = useRef(null);

  let carouselthreeone = useRef(null);
  let carouselthreetwo = useRef(null);
  let carouselthreethree = useRef(null);
  let carouselthreefour = useRef(null);
  let carouselthreefive = useRef(null);

  let carouselfourthone = useRef(null);

  let carouselfifthone = useRef(null);
  let carouselfifthtwo = useRef(null);
  let carouselfifththree = useRef(null);

  let carouselsixthone = useRef(null);
  let carouselsixthtwo = useRef(null);

  let carouselsevenone = useRef(null);
  let carouselseventwo = useRef(null);
  let carouselseventhree = useRef(null);

  const [visitedSlides, setVisitedSlides] = useState([]);

  const onSlideChange = (slick, currentSlide) => {
    resetDotColors();

    updateDotColor(currentSlide);
  };

  const resetDotColors = () => {
    for (let i = 0; i <= 7; i++) {
      const dot = document.querySelector(`.dotsfill${i}`);
      if (dot) {
        dot.style.fill = "rgb(0, 146, 255)";
      }
    }
  };

  const updateDotColor = (currentSlide) => {
    const dot = document.querySelector(`.dotsfill${currentSlide}`);

    if (dot) {
      dot.style.fill = "red";
    }

    setVisitedSlides((prevVisitedSlides) => [
      ...new Set([...prevVisitedSlides, currentSlide]),
    ]);
  };

  useEffect(() => {
    updateDotColor(0);
  }, []);

  // function svgbackgroundcolor(currentSlide) {
  //   const svgcolor = document.querySelector(".svg-color");
  //   if (currentSlide === 0) {
  //     svgcolor.style.backgroundColor = "#0e103a";
  //   } else if (currentSlide === 1) {
  //     svgcolor.style.backgroundColor = "#142136";
  //   } else if (currentSlide === 2) {
  //     svgcolor.style.backgroundColor = "#135fda";
  //   } else if (currentSlide === 3) {
  //     svgcolor.style.backgroundColor = "#052889";
  //   } else if (currentSlide === 4) {
  //     svgcolor.style.backgroundColor = "#347344";
  //   } else if (currentSlide === 5) {
  //     svgcolor.style.backgroundColor = "#9d43ba";
  //   } else if (currentSlide === 6) {
  //     svgcolor.style.backgroundColor = "#6437a5";
  //   }
  // }

  useEffect(() => {
    // if (isVertical) {
    onSlideChange(currentSlide);
    // svgbackgroundcolor(currentSlide);
    console.log(currentSlide);

    ref.current.forEach((el, index) => {
      gsap.fromTo(
        el,
        { opacity: 0, y: -500, rotateX: 100 },
        {
          opacity: 1,
          y: 0,
          rotateX: 0,
          duration: 0.5,
          ease: "power1.out",
          scrollTrigger: {
            trigger: el,
            start: "top bottom",
            toggleActions: "play none none reverse",
          },
        }
      );
    });

    pararef.current.forEach((el, index) => {
      gsap.fromTo(
        el,
        {
          autoAlpha: 0,
          y: -5,
          rotateX: 100,
        },
        {
          autoAlpha: 1,
          y: 0,
          rotateX: 0,
          duration: 0.8,
          ease: "power1.out",
          delay: 0.2,
        }
      );
    });

    anchorref.current.forEach((el, index) => {
      gsap.fromTo(
        el,
        {
          autoAlpha: 0,
          y: -5,
          rotateX: 100,
        },
        {
          autoAlpha: 1,
          y: 0,
          rotateX: 0,
          duration: 0.8,
          ease: "power1.out",
          delay: 0.3,
        }
      );
    });

    if (currentSlide === 0) {
      gsap.from(carouseoneimgdown, {
        autoAlpha: 1,
        y: 100,
        duration: 0.8,
        ease: "power3.out",
      });
      gsap.from(carouseoneimgup, {
        autoAlpha: 1,
        y: -500,
        duration: 0.8,
        ease: "power3.out",
      });
    } else if (currentSlide === 1) {
      gsap.from(carousetwoimgone, {
        autoAlpha: 0,
        y: 200,
        duration: 0.8,
        ease: "power1.out",
        delay: 0.3,
      });
      gsap.from(carousetwoimgtwo, {
        autoAlpha: 0,
        y: -500,
        duration: 0.8,
        ease: "power1.out",
        delay: 0.3,
      });
      gsap.from(carousetwoimgthree, {
        autoAlpha: 0,
        y: 500,
        duration: 0.8,
        ease: "power1.out",
        delay: 0.3,
      });
      gsap.from(carousetwoimgfour, {
        autoAlpha: 0,
        x: 500,
        duration: 0.8,
        ease: "power1.out",
        delay: 0.3,
      });
      gsap.from(carousetwoimgfive, {
        autoAlpha: 0,
        x: -500,
        duration: 0.8,
        ease: "power1.out",
        delay: 0.3,
      });
      gsap.from(carousetwoimgsix, {
        autoAlpha: 0,
        x: 500,
        duration: 0.8,
        ease: "power1.out",
        delay: 0.3,
      });
      gsap.from(carousetwoimgseven, {
        autoAlpha: 0,
        x: -500,
        duration: 0.8,
        ease: "power1.out",
        delay: 0.3,
      });
    } else if (currentSlide === 2) {
      gsap.from(carouselthreeone, {
        autoAlpha: 0,
        y: -50,
        duration: 0.8,
        ease: "power1.out",
        delay: 0.3,
      });
      gsap.from(carouselthreetwo, {
        autoAlpha: 0,
        y: 100,
        duration: 0.8,
        ease: "power1.out",
        delay: 0.3,
      });
      gsap.from(carouselthreethree, {
        autoAlpha: 0,
        y: 50,
        duration: 0.8,
        ease: "power1.out",
        delay: 0.3,
      });
      gsap.from(carouselthreefour, {
        autoAlpha: 0,
        y: 100,
        duration: 0.8,
        ease: "power1.out",
        delay: 0.3,
      });
      gsap.from(carouselthreefive, {
        autoAlpha: 0,
        y: 500,
        duration: 0.8,
        ease: "power1.out",
        delay: 0.3,
      });
    } else if (currentSlide === 3) {
      gsap.from(carouselfourthone, {
        autoAlpha: 0,
        y: 500,
        duration: 0.5,
        ease: "power4.out",
        delay: 0.3,
      });
    } else if (currentSlide === 4) {
      gsap.from(carouselfifthone, {
        autoAlpha: 0,
        y: 500,
        duration: 0.5,
        ease: "power4.out",
        delay: 0.3,
      });
      gsap.from(carouselfifthtwo, {
        autoAlpha: 0,
        y: 50,
        duration: 1,
        ease: "power4.out",
        delay: 0.3,
      });
      gsap.from(carouselfifththree, {
        autoAlpha: 0,
        y: 500,
        duration: 1,
        ease: "bounce.out",
        delay: 0.3,
      });
    } else if (currentSlide === 5) {
      gsap.from(carouselsixthone, {
        autoAlpha: 0,
        y: 500,
        duration: 0.5,
        ease: "power4.out",
        delay: 0.3,
      });
      gsap.from(carouselsixthtwo, {
        autoAlpha: 0,
        y: -500,
        duration: 0.5,
        ease: "power2.out",
        delay: 0.3,
      });
    } else if (currentSlide === 6) {
      gsap.from(carouselsevenone, {
        autoAlpha: 0,
        x: 100,
        duration: 0.5,
        ease: "bounce.out",
        delay: 0.3,
      });
      gsap.from(carouselseventwo, {
        autoAlpha: 0,
        y: -500,
        duration: 0.5,
        ease: "power2.out",
        delay: 0.3,
      });
      gsap.from(carouselseventhree, {
        autoAlpha: 0,
        y: 500,
        duration: 0.5,
        ease: "power2.out",
        delay: 0.3,
      });
    }
    // }
  }, [currentSlide, isVertical]);

  const addtoRefs = (el) => {
    if (el && !ref.current.includes(el)) {
      ref.current.push(el);
    }
  };

  const addparaRefs = (el) => {
    if (el && !pararef.current.includes(el)) {
      pararef.current.push(el);
    }
  };

  const addanchorRefs = (el) => {
    if (el && !anchorref.current.includes(el)) {
      anchorref.current.push(el);
    }
  };

  const handleAfterChange = (currentSlide) => {};

  const sliderSettings = {
    dots: isVertical ? false : true,
    infinite: false,
    arrows: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    vertical: isVertical,
    verticalSwiping: isVertical,
    afterChange: handleAfterChange,
    // beforeChange: onSlideChange,
    beforeChange: (oldIndex, newIndex) => setCurrentSlide(newIndex),
  };
  return (
    <WrapperStyled>
      <Slider {...sliderSettings} ref={sliderRef}>
        <div className="carouselwrapper carouselfirst">
          <div className="image d-flex position-relative">
            <img
              className="common-img position-absolute fitneddimgSecond"
              src={fitneddimgSecond}
              alt=""
              // ref={(el) => (carouseoneimgdown = el)}
              ref={addtoRefs}
            />
            <img
              className="common-img fitnesssecond position-absolute"
              src={fitneddimg}
              alt=""
              // ref={(el) => (carouseoneimgup = el)}
              ref={addtoRefs}
            />
          </div>
          <div className="content  d-flex justify-content-center flex-column">
            <h1 ref={addtoRefs}>Welcome to MS Motors!</h1>
            <p ref={addparaRefs}>
              At MS Motors, we're dedicated to providing you with the best
              selection of quality pre-owned vehicles. Whether you're in the
              market for a sleek sedan, a spacious SUV, or a rugged truck, we
              have something for everyone. With our commitment to excellence and
              customer satisfaction, you can trust us for all your car-buying
              needs.
            </p>
            {/* <a href="" ref={addanchorRefs}>
              CASE STUDY- <button>Coming Soon</button>
            </a> */}
          </div>
        </div>
        {/* 2nd carousel */}
        <div className="carouselwrapper carouselsecond">
          <div className="image d-flex position-relative">
            <img
              className="common-img  position-absolute pizza3"
              src={pizza3}
              alt=""
              ref={(el) => (carousetwoimgone = el)}
            />
            <img
              className="common-img position-absolute pizza1"
              src={pizza1}
              alt=""
              ref={(el) => (carousetwoimgtwo = el)}
            />
            <img
              className="common-img position-absolute pizza2"
              src={pizza2}
              alt=""
              ref={(el) => (carousetwoimgthree = el)}
            />
            <img
              className="common-img position-absolute pizza4"
              src={pizza4}
              alt=""
              ref={(el) => (carousetwoimgfour = el)}
            />
            <img
              className="common-img position-absolute pizza5"
              src={pizza5}
              alt=""
              ref={(el) => (carousetwoimgfive = el)}
            />
            <img
              className="common-img position-absolute pizza6"
              src={pizza6}
              alt=""
              ref={(el) => (carousetwoimgsix = el)}
            />
            <img
              className="common-img position-absolute pizza7"
              src={pizza7}
              alt=""
              ref={(el) => (carousetwoimgseven = el)}
            />
          </div>
          <div className="content d-flex justify-content-center flex-column">
            <h1 ref={addtoRefs}>Our Mission</h1>
            <p ref={addparaRefs}>
              At MS Motors, our mission is simple: to offer our customers a
              hassle-free car-buying experience. We aim to exceed expectations
              by providing top-notch customer service, transparent pricing, and
              a wide selection of quality vehicles. Your satisfaction is our
              priority, and we strive to make every interaction with us a
              positive one.
            </p>
            {/* <a ref={addanchorRefs} href="">
              View Case Study
            </a> */}
          </div>
        </div>
        {/* 3rd carousel */}
        <div className="carouselwrapper carouselthird">
          <div className="image d-flex position-relative">
            <img
              className="common-img position-absolute thirdcarouseone"
              src={thirdcarouseone}
              alt=""
              ref={(el) => (carouselthreeone = el)}
            />
            <img
              className="common-img position-absolute thirdcarousetwo"
              src={thirdcarousetwo}
              alt=""
              ref={(el) => (carouselthreetwo = el)}
            />
            <img
              className="common-img position-absolute thirdcarousethree"
              src={thirdcarousethree}
              alt=""
              ref={(el) => (carouselthreethree = el)}
            />
            <img
              className="common-img position-absolute thirdcarousefour"
              src={thirdcarousefour}
              alt=""
              ref={(el) => (carouselthreefour = el)}
            />
            <img
              className="common-img position-absolute thirdcarousefive"
              src={thirdcarousefive}
              alt=""
              ref={(el) => (carouselthreefive = el)}
            />
          </div>
          <div className="content d-flex justify-content-center flex-column">
            <h1 ref={addtoRefs}>Our Inventory</h1>
            <p ref={addparaRefs}>
              Explore our extensive inventory of pre-owned vehicles, carefully
              selected to meet a variety of preferences and budgets. From
              fuel-efficient compact cars to luxurious sedans and powerful
              trucks, we have something to suit every lifestyle. Browse our
              inventory online or visit our showroom to see our latest arrivals.
            </p>
            {/* <a ref={addanchorRefs} href="">
              View Case Study
            </a> */}
          </div>
        </div>
        {/* 4th carousel */}
        <div className="carouselwrapper carouselfourth">
          <div className="image d-flex position-relative">
            <img
              className="common-img fitnessone"
              src={fourthcarouseone}
              alt=""
              ref={(el) => (carouselfourthone = el)}
            />
          </div>
          <div className="content d-flex justify-content-center flex-column">
            <h1 ref={addtoRefs}>Financing Options</h1>
            <p ref={addparaRefs}>
              We understand that financing is an essential part of the
              car-buying process. That's why we offer flexible financing options
              tailored to your individual needs. Whether you have excellent
              credit, less-than-perfect credit, or no credit history at all, our
              team of finance experts is here to help you secure the best
              possible terms and rates.
            </p>
            {/* <a ref={addanchorRefs} href="">
              View Case Study
            </a> */}
          </div>
        </div>

        {/* 5th carousel */}
        <div className="carouselwrapper carouselfive">
          <div className="image d-flex position-relative">
            <img
              className="common-img position-absolute fifthcarouseone"
              src={fifthcarouseone}
              alt=""
              ref={(el) => (carouselfifthone = el)}
            />
            <img
              className="common-img position-absolute fifthcarousetwo"
              src={fifthcarousetwo}
              alt=""
              ref={(el) => (carouselfifthtwo = el)}
            />
            <img
              className="common-img position-absolute fifthcarousethree"
              src={fifthcarousethree}
              alt=""
              ref={(el) => (carouselfifththree = el)}
            />
          </div>
          <div className="content d-flex justify-content-center flex-column">
            <h1 ref={addtoRefs}>Trade-In Program</h1>
            <p ref={addparaRefs}>
              Looking to trade in your current vehicle? Take advantage of our
              hassle-free trade-in program. Our experienced appraisers will
              assess the value of your trade-in and offer you a fair price. Put
              the value of your old car towards the purchase of a quality
              pre-owned vehicle from MS Motors.
            </p>
            {/* <a ref={addanchorRefs} href="">
              View Case Study
            </a> */}
          </div>
        </div>

        {/* 6th carousel */}
        <div className="carouselwrapper carouselsix">
          <div className="image d-flex position-relative">
            <img
              className="common-img position-absolute sixthcarouseone"
              src={sixthcarouseone}
              alt=""
              ref={(el) => (carouselsixthone = el)}
            />
            <img
              className="common-img position-absolute sixthcarousetwo"
              src={sixthcarousetwo}
              alt=""
              ref={(el) => (carouselsixthtwo = el)}
            />
          </div>
          <div className="content d-flex justify-content-center flex-column">
            <h1 ref={addtoRefs}>Certified Pre-Owned</h1>
            <p ref={addparaRefs}>
              For added peace of mind, consider our certified pre-owned (CPO)
              vehicles. Each CPO vehicle undergoes a rigorous inspection process
              to ensure its quality, reliability, and safety. With a
              comprehensive warranty and roadside assistance included, you can
              drive away confidently knowing that your investment is protected.
            </p>
            {/* <a ref={addanchorRefs} href="">
              View Case Study
            </a> */}
          </div>
        </div>

        {/* 7th carousel */}
        <div className="carouselwrapper carouselseven">
          <div className="image d-flex position-relative">
            <img
              className="common-img position-absolute seventhcarouseone"
              src={seventhcarouseone}
              alt=""
              ref={(el) => (carouselsevenone = el)}
            />
            <img
              className="common-img position-absolute seventhcarousetwo"
              src={seventhcarousetwo}
              alt=""
              ref={(el) => (carouselseventwo = el)}
            />
            <img
              className="common-img position-absolute seventhcarousethree"
              src={seventhcarousethree}
              alt=""
              ref={(el) => (carouselseventhree = el)}
            />
          </div>
          <div className="content d-flex justify-content-center flex-column">
            <h1 ref={addtoRefs}>Contact Us</h1>
            <p ref={addparaRefs}>
              Ready to find your next vehicle? Contact us today to schedule a
              test drive or inquire about our current inventory. Our friendly
              and knowledgeable team is here to assist you every step of the
              way. Visit our showroom, give us a call, or send us a message
              online. We look forward to helping you find the perfect car!
            </p>
            {/* <a ref={addanchorRefs} href="">
              View Case Study
            </a> */}
          </div>
        </div>
      </Slider>
      {isVertical ? <LogoMS /> : ""}
    </WrapperStyled>
  );
}

export default Landing;
